/**
 * vanywhere-token
 * Created by ordemri
 * on 5/8/18
 */
import assertRevert from "./helpers/assertRevert";


const BigNumber = web3.BigNumber;
require('babel-register');
require('babel-polyfill');
import expectThrow from './helpers/expectThrow';
import ether from './helpers/ether'
import latestTime from "./helpers/latestTime";
import {increaseTimeTo, duration} from "./helpers/increaseTime";

require('chai')
    .use(require('chai-as-promised'))
    .use(require('chai-bignumber')(BigNumber))
    .should();


var tokenContract = artifacts.require("./VanywhereToken.sol");
var accessControlContract = artifacts.require("./AccessControl.sol");
var crowdsaleContract = artifacts.require("./VanywhereCrowdsale.sol");


contract('VanywhereToken', function ([admin, other1, k1, k2, a1, a2, vany, other2]) {


    let token;
    let sale;
    let acm;
    let cap = ether(800000000);
    let _name = 'VANY';
    let _symbol = 'VANY';
    const _decimals = 18;
    let openingTime = latestTime() + duration.days(1);
    let endTime = openingTime + duration.days(30);

    //uint256 _startRate, address _wallet, ERC20 _token, uint256 _openingTime, uint256 _closingTime, AccessControl addr
    beforeEach(async function () {
            acm = await accessControlContract.new({from: admin});
            token = await tokenContract.new(acm.address, cap, _name, _symbol, _decimals);
            sale = await crowdsaleContract.new(0, vany, token.address, openingTime, endTime, acm.address, {from: admin});
            const result = await acm.addAddressesToAccessControl([admin, other1, sale.address, k1, k2, a1, a2], [9, 8, 8, 1, 1, 2, 2]);

        }
    )

    describe('Detailed QRC20 token', function () {
        it('has a name', async function () {
            const name = await token.name();
            name.should.be.equal(_name);
        });

        it('has a symbol', async function () {
            const symbol = await token.symbol();
            symbol.should.be.equal(_symbol);
        });

        it('has an amount of decimals', async function () {
            const decimals = await token.decimals();
            decimals.should.be.bignumber.equal(_decimals);
        });
    });


    describe('total supply', function () {
        it('returns the cap of total tokens', async function () {
            const total = await token.cap();

            assert(cap.eq(total));

        });
    });

    describe('Access Manager', function () {

        it('should start with the correct role', async function () {
            let role = await acm.getRole(admin);
            assert.equal(9, parseInt(role));
        });

    });

    describe('crowdasle', function () {
        it('should reject the payment before sale started - admin', async function () {
            let amount = 30000;
            assertRevert(sale.sendTransaction({from: admin, value: amount}));
        });

        it('should reject the payment before sale started - kyc', async function () {
            let amount = 30000;
            assertRevert(sale.sendTransaction({from: k1, value: amount}));
        });

        it('should reject the payment before sale started - accredited', async function () {
            let amount = 30000;
            assertRevert(sale.sendTransaction({from: a1, value: amount}));
        });

        it('should reject the payment before sale started - unauthorized', async function () {
            let amount = 30000;
            assertRevert(sale.sendTransaction({from: other2, value: amount}));

        });

        describe('Stage one', function () {
            it('should accept payments after start - kyc', async function () {
                await increaseTimeTo(openingTime + 1);
                let amount = 30000;
                const balance = await token.balanceOf(k2);
                let {logs} = await sale.sendTransaction({from: k2, value: amount});
                const balance1 = await token.balanceOf(k2);
                assert(balance.eq(0));
                assert(balance1.eq(400 * amount));
            });


            it('should accept payments after start - kyc', async function () {
                let amount = 30000;
                const balance = await token.balanceOf(k2);
                let {logs} = await sale.sendTransaction({from: k2, value: amount});
                const balance1 = await token.balanceOf(k2);
                assert(balance.eq(0));
                assert(balance1.eq(400 * amount));
            });

            it('should accept payments after start - accredited', async function () {

                let amount = 30000;
                const balance = await token.balanceOf(a1);
                let {logs} = await sale.sendTransaction({from: a1, value: amount});
                const balance1 = await token.balanceOf(a1);
                assert(balance.eq(0));
                assert(balance1.eq(400 * amount));
            });
        });

        describe('Stage Two', function () {
            it('should accept payments - kyc', async function () {
                increaseTimeTo(openingTime + 1 + duration.hours(5));

                let amount = 30000;
                const balance = await token.balanceOf(k2);
                let {logs} = await sale.sendTransaction({from: k2, value: amount});
                const balance1 = await token.balanceOf(k2);;
                assert(balance.eq(0));
                // assert(balance1.eq(395 * amount));
            });

            it('should accept payments - accredited', async function () {
                let amount = 30000;
                const balance = await token.balanceOf(a2);
                let {logs} = await sale.sendTransaction({from: a2, value: amount});
                const balance1 = await token.balanceOf(a2);
                assert(balance.eq(0));
                assert(balance1.eq(395 * amount));
            });


            it('should reject payments after start - unauthorized', async function () {

                let amount = 30000;
                assertRevert(sale.buyTokens(other2, {value: amount, from: k2}));
            });
        });

        describe('Stage Three', function () {
            let rateStage3 = 390;
            it('should accept payments - kyc', async function () {
                increaseTimeTo(openingTime + 1 + duration.hours(72));

                let amount = 30000;
                const balance = await token.balanceOf(k2);
                let {logs} = await sale.sendTransaction({from: k2, value: amount});
                const balance1 = await token.balanceOf(k2);
                assert(balance.eq(0));
                assert(balance1.eq(rateStage3 * amount));
            });

            it('should accept payments - accredited', async function () {
                let amount = 30000;
                const balance = await token.balanceOf(a1);
                let {logs} = await sale.sendTransaction({from: a1, value: amount});
                const balance1 = await token.balanceOf(a1);
                assert(balance.eq(0));
                assert(balance1.eq(rateStage3 * amount));
            });


            it('should reject payments after start - admin', async function () {

                let amount = 30000;
                assertRevert(sale.buyTokens(other2, {value: amount, from: admin}));
            });

            it('should reject payments after start - unauthorized', async function () {

                let amount = 30000;
                assertRevert(sale.buyTokens(other2, {value: amount, from: k2}));
            });
        });


        describe('Stage Four', function () {
            let rateStage4 = 385;
            it('changing to stage 4 - should accept payments - kyc', async function () {
                increaseTimeTo(openingTime + 1 + duration.weeks(2));

                let amount = 30000;
                const balance = await token.balanceOf(k1);
                let {logs} = await sale.sendTransaction({from: k1, value: amount});
                const balance1 = await token.balanceOf(k1);
                assert(balance.eq(0));
                assert(balance1.eq(rateStage4 * amount));
            });

            it('should accept payments - accredited', async function () {
                let amount = 30000;
                const balance = await token.balanceOf(a1);
                let {logs} = await sale.sendTransaction({from: a1, value: amount});
                const balance1 = await token.balanceOf(a1)
                assert(balance.eq(0));
                assert(balance1.eq(rateStage4 * amount));
            });

            it('should accept payments - kyc', async function () {
                let amount = 30000;
                const balance = await token.balanceOf(k1);
                let {logs} = await sale.sendTransaction({from: k1, value: amount});
                const balance1 = await token.balanceOf(k1);
                assert(balance.eq(0));
                assert(balance1.eq(rateStage4 * amount));
            });

            it('should reject payments after start - unauthorized', async function () {

                let amount = 30000;
                assertRevert(sale.buyTokens(other2, {value: amount, from: k2}));
            });
        });


        it('should reject payments after cap reached', async function () {
            let amount1 = 799900000 * 10 ** 18;
            await token.mint(other1, amount1);

            let amount = ether(1000);
            const balance = await token.balanceOf(k2);
            assertRevert(sale.buyTokens(k2, {value: amount, from: k2}));

        });


    });

    describe('minting with cap', function () {
        it('check total supply after minting', async function () {
            let amount = 700000000 * 10 ** 18;
            await token.mint(other1, amount);

            const balance = await token.balanceOf(other1);
            assert.equal(balance, amount);
        });


        it('should fail to mint to un authorized user', async function () {
            let amount = 8000 * 10 ** 18;
            assertRevert(token.mint(other1, amount, {from: admin}));
        });

        it('mints the for other user amount', async function () {
            let amount = 100000000 * 10 ** 18;
            await token.mint(admin, amount);

            const balance = await token.balanceOf(admin);
            const total = await token.totalSupply();
            assert.equal(balance, amount);
            assert.equal(total, amount);
        });

        it('mints overflow', async function () {
            let amount = 800000001 * 10 ** 18;
            expectThrow(token.mint(other1, amount));
        });

        it('should fail to mint', async function () {
            let amount = 800000001 * 10 ** 18;
            expectThrow(token.mint(other1, amount, {from: a1}));
        });


    });

    describe('token locks', function () {

        describe('before sale finished', function () {
            it('should fail the mint', async function () {
                let amount = 30000;
                assertRevert(token.mint(admin, amount, {from: a1}));
            });

            it('should fail transfer accredited <-> accredited after the sale starts- lock period', async function () {

                let amount = 40000000000000;


                await token.mint(a1, amount, {from: admin});
                assertRevert(token.transfer(a2, amount, {from: a1}));
            });

            it('should fail transfer accredited <-> unauthorized after the sale starts', async function () {

                let amount = 40000000000000;


                await token.mint(a1, amount, {from: admin});
                assertRevert(token.transfer(other2, amount, {from: a1}));
            });

            it('should fail transfer kyc <-> unauthorized after the sale starts', async function () {

                let amount = 40000000000000;


                await token.mint(k1, amount, {from: admin});
                assertRevert(token.transfer(other2, amount, {from: k1}));
            });

            it('should fail transfer kyc <-> unauthorized after the sale starts', async function () {

                let amount = 40000000000000;


                await token.mint(admin, amount, {from: admin});
                assertRevert(token.transfer(other2, amount, {from: admin}));
            });

            it('should fail transfer unauthorized <-> unauthorized after the sale starts', async function () {

                let amount = 40000000000000;

                assertRevert(token.transfer(other2, amount, {from: other2}));
            });

            it('should fail transfer unauthorized <-> kyc after the sale starts', async function () {


                let amount = 40000000000000;


                assertRevert(token.transfer(k1, amount, {from: other2}));
            });

            it('should fail transfer unauthorized <-> accredited after the sale starts', async function () {

                let amount = 40000000000000;

                assertRevert(token.transfer(a1, amount, {from: other2}));
            });

            it('should fail transfer unauthorized <-> admin after the sale starts', async function () {

                let amount = 40000000000000;

                assertRevert(token.transfer(admin, amount, {from: other2}));
            });


            it('should fail transfer accredited <-> kyc after the sale starts', async function () {

                let amount = 40000000000000;
                await token.mint(a1, amount, {from: admin});
                assertRevert(token.transfer(k1, amount, {from: a1}));


            });

        });


        describe('sale finished', function () {
            it('fail finish the sale for not admin', async function () {

                expectThrow(token.finishTokenSale({from: a2}));
                const tokenSaleFinished = await token.tokenSaleFinished();
                const tokenSaleEndTime = await token.tokenSaleEndTime();
                assert(!tokenSaleFinished);
                assert(tokenSaleEndTime.eq(0))
            });

            it('should finish the sale', async function () {

                await token.finishTokenSale({from: admin});
                const tokenSaleFinished = await token.tokenSaleFinished();
                const tokenSaleEndTime = await token.tokenSaleEndTime();
                assert(tokenSaleFinished);
                assert(!tokenSaleEndTime.eq(0))
            });


            it('should fail transfer accredited <-> accredited after the sale ends- lock period', async function () {

                await token.finishTokenSale({from: admin});
                const tokenSaleFinished = await token.tokenSaleFinished();
                const tokenSaleEndTime = await token.tokenSaleEndTime();
                assert(tokenSaleFinished);
                assert(!tokenSaleEndTime.eq(0))
                let amount = 40000000000000;


                await token.mint(a1, amount, {from: admin});
                assertRevert(token.transfer(a2, amount, {from: a1}));
            });

            it('should fail transfer accredited <-> unauthorized after the sale ends', async function () {

                await token.finishTokenSale({from: admin});
                const tokenSaleFinished = await token.tokenSaleFinished();
                const tokenSaleEndTime = await token.tokenSaleEndTime();
                assert(tokenSaleFinished);
                assert(!tokenSaleEndTime.eq(0))
                let amount = 40000000000000;


                await token.mint(a1, amount, {from: admin});
                assertRevert(token.transfer(other2, amount, {from: a1}));
            });

            it('should fail transfer kyc <-> unauthorized after the sale ends', async function () {

                await token.finishTokenSale({from: admin});
                const tokenSaleFinished = await token.tokenSaleFinished();
                const tokenSaleEndTime = await token.tokenSaleEndTime();
                assert(tokenSaleFinished);
                assert(!tokenSaleEndTime.eq(0))
                let amount = 40000000000000;


                await token.mint(k1, amount, {from: admin});
                assertRevert(token.transfer(other2, amount, {from: k1}));
            });

            it('should fail transfer kyc <-> unauthorized after the sale ends', async function () {

                await token.finishTokenSale({from: admin});
                const tokenSaleFinished = await token.tokenSaleFinished();
                const tokenSaleEndTime = await token.tokenSaleEndTime();
                assert(tokenSaleFinished);
                assert(!tokenSaleEndTime.eq(0))
                let amount = 40000000000000;


                await token.mint(admin, amount, {from: admin});
                assertRevert(token.transfer(other2, amount, {from: admin}));
            });

            it('should fail transfer unauthorized <-> unauthorized after the sale ends', async function () {

                await token.finishTokenSale({from: admin});
                const tokenSaleFinished = await token.tokenSaleFinished();
                const tokenSaleEndTime = await token.tokenSaleEndTime();
                assert(tokenSaleFinished);
                assert(!tokenSaleEndTime.eq(0))
                let amount = 40000000000000;

                assertRevert(token.transfer(other2, amount, {from: other2}));
            });

            it('should fail transfer unauthorized <-> kyc after the sale ends', async function () {

                await token.finishTokenSale({from: admin});
                const tokenSaleFinished = await token.tokenSaleFinished();
                const tokenSaleEndTime = await token.tokenSaleEndTime();
                assert(tokenSaleFinished);
                assert(!tokenSaleEndTime.eq(0))
                let amount = 40000000000000;


                assertRevert(token.transfer(k1, amount, {from: other2}));
            });

            it('should fail transfer unauthorized <-> accredited after the sale ends', async function () {

                await token.finishTokenSale({from: admin});
                const tokenSaleFinished = await token.tokenSaleFinished();
                const tokenSaleEndTime = await token.tokenSaleEndTime();
                assert(tokenSaleFinished);
                assert(!tokenSaleEndTime.eq(0))
                let amount = 40000000000000;

                assertRevert(token.transfer(a1, amount, {from: other2}));
            });

            it('should fail transfer unauthorized <-> admin after the sale ends', async function () {

                await token.finishTokenSale({from: admin});
                const tokenSaleFinished = await token.tokenSaleFinished();
                const tokenSaleEndTime = await token.tokenSaleEndTime();
                assert(tokenSaleFinished);
                assert(!tokenSaleEndTime.eq(0))
                let amount = 40000000000000;

                assertRevert(token.transfer(admin, amount, {from: other2}));
            });


            it('should fail transfer accredited <-> kyc after the sale ends', async function () {

                await token.finishTokenSale({from: admin});
                const tokenSaleFinished = await token.tokenSaleFinished();
                const tokenSaleEndTime = await token.tokenSaleEndTime();
                assert(tokenSaleFinished);
                assert(!tokenSaleEndTime.eq(0))
                let amount = 11233;

                await token.mint(a1, amount, {from: admin});
                assertRevert(token.transfer(k1, amount, {from: a1}));

                const senderBalance = await token.balanceOf(a1);
                assert(senderBalance.eq(amount));

                const receiverBalance = await token.balanceOf(k1);
                assert(receiverBalance.eq(0));


            });

            describe('3 months after the sale ends', function () {

                it('transfer accredited <-> accredited', async function () {

                    await token.finishTokenSale({from: admin});
                    const tokenSaleFinished = await token.tokenSaleFinished();
                    const tokenSaleEndTime = await token.tokenSaleEndTime();
                    let afterLockup = parseInt(tokenSaleEndTime) + duration.days(90) + 10;
                    await increaseTimeTo(afterLockup);

                    assert(tokenSaleFinished);
                    assert(!tokenSaleEndTime.eq(0))
                    let amount = 40000000000000;


                    await token.mint(a1, amount, {from: admin});
                    await token.transfer(a2, amount, {from: a1});
                    const receiverBalance = await token.balanceOf(a2);
                    assert(receiverBalance.eq(amount))
                });

                it('transfer kyc <-> kyc', async function () {

                    await token.finishTokenSale({from: admin});
                    const tokenSaleEndTime = await token.tokenSaleEndTime();
                    assert(!tokenSaleEndTime.eq(0))
                    let afterLockup = parseInt(tokenSaleEndTime) + duration.days(90) + 10;
                    await increaseTimeTo(afterLockup);
                    let amount = 20001;


                    await token.mint(k1, amount, {from: admin});
                    await token.transfer(k2, amount, {from: k1});


                    const receiverBalance = await token.balanceOf(k2);
                    assert(receiverBalance.eq(amount))
                });

                it('transfer admin <-> kyc', async function () {

                    await token.finishTokenSale({from: admin});
                    const tokenSaleEndTime = await token.tokenSaleEndTime();
                    assert(!tokenSaleEndTime.eq(0))
                    let afterLockup = parseInt(tokenSaleEndTime) + duration.days(90) + 10;
                    await increaseTimeTo(afterLockup);
                    let amount = 30001;


                    await token.mint(admin, amount, {from: admin});
                    await token.transfer(k1, amount, {from: admin});


                    const receiverBalance = await token.balanceOf(k1);
                    assert(receiverBalance.eq(amount))
                });

                it('fail transfer kyc <-> accredited', async function () {

                    await token.finishTokenSale({from: admin});
                    const tokenSaleEndTime = await token.tokenSaleEndTime();
                    assert(!tokenSaleEndTime.eq(0))
                    let afterLockup = parseInt(tokenSaleEndTime) + duration.days(90) + 10;
                    await increaseTimeTo(afterLockup);
                    let amount = 50001;


                    await token.mint(k1, amount, {from: admin});
                    const beforeBalance = await token.balanceOf(a1);
                    await token.transfer(a1, amount, {from: k1});
                    const receiverBalance = await token.balanceOf(a1);
                    assert(receiverBalance.eq(beforeBalance))
                });

                it('fail transfer kyc <-> unauthorized', async function () {

                    await token.finishTokenSale({from: admin});
                    const tokenSaleEndTime = await token.tokenSaleEndTime();
                    assert(!tokenSaleEndTime.eq(0))
                    let afterLockup = parseInt(tokenSaleEndTime) + duration.days(90) + 10;
                    await increaseTimeTo(afterLockup);
                    let amount = 50001;


                    await token.mint(k1, amount, {from: admin});
                    assertRevert(token.transfer(other2, amount, {from: k1}));
                });

                it('fail transfer accredited <-> unauthorized', async function () {

                    await token.finishTokenSale({from: admin});
                    const tokenSaleEndTime = await token.tokenSaleEndTime();
                    assert(!tokenSaleEndTime.eq(0))
                    let afterLockup = parseInt(tokenSaleEndTime) + duration.days(90) + 10;
                    await increaseTimeTo(afterLockup);
                    let amount = 50001;


                    await token.mint(a1, amount, {from: admin});
                    assertRevert(token.transfer(other2, amount, {from: a1}));
                });

                it('fail transfer unauthorized <-> accredited', async function () {

                    await token.finishTokenSale({from: admin});
                    const tokenSaleEndTime = await token.tokenSaleEndTime();
                    assert(!tokenSaleEndTime.eq(0))
                    let afterLockup = parseInt(tokenSaleEndTime) + duration.days(90) + 10;
                    await increaseTimeTo(afterLockup);
                    let amount = 50001;


                    assertRevert(token.transfer(a1, amount, {from: other2}));
                });

                it('transfer unauthorized <-> kyc', async function () {

                    await token.finishTokenSale({from: admin});
                    const tokenSaleEndTime = await token.tokenSaleEndTime();
                    assert(!tokenSaleEndTime.eq(0))
                    let afterLockup = parseInt(tokenSaleEndTime) + duration.days(90) + 10;
                    await increaseTimeTo(afterLockup);
                    let amount = 50001;

                    await token.mint(other2, amount, {from: admin});
                    await token.transfer(k1, amount, {from: other2});
                    const receiverBalance = await token.balanceOf(k1);
                    assert(receiverBalance.eq(amount))

                });

                it('fail transfer unauthorized <-> admin', async function () {

                    await token.finishTokenSale({from: admin});
                    const tokenSaleEndTime = await token.tokenSaleEndTime();
                    assert(!tokenSaleEndTime.eq(0))
                    let afterLockup = parseInt(tokenSaleEndTime) + duration.days(90) + 10;
                    await increaseTimeTo(afterLockup);
                    let amount = 50001;


                    assertRevert(token.transfer(admin, amount, {from: other2}));
                });

                it('fail transfer unauthorized <-> unauthorized', async function () {

                    await token.finishTokenSale({from: admin});
                    const tokenSaleEndTime = await token.tokenSaleEndTime();
                    assert(!tokenSaleEndTime.eq(0))
                    let afterLockup = parseInt(tokenSaleEndTime) + duration.days(90) + 10;
                    await increaseTimeTo(afterLockup);
                    let amount = 50001;


                    assertRevert(token.transfer(other2, amount, {from: other2}));
                });


                it('fail transfer kyc <-> admin', async function () {

                    await token.finishTokenSale({from: admin});
                    const tokenSaleEndTime = await token.tokenSaleEndTime();
                    assert(!tokenSaleEndTime.eq(0))
                    let afterLockup = parseInt(tokenSaleEndTime) + duration.days(90) + 10;
                    await increaseTimeTo(afterLockup);
                    let amount = 50001;


                    await token.mint(admin, amount, {from: admin});
                    assertRevert(token.transfer(other2, amount, {from: admin}));
                });


                it('fail transfer accredited <-> kyc', async function () {

                    await token.finishTokenSale({from: admin});
                    const tokenSaleEndTime = await token.tokenSaleEndTime();
                    assert(!tokenSaleEndTime.eq(0))
                    let afterLockup = parseInt(tokenSaleEndTime) + duration.days(90) + 10;
                    await increaseTimeTo(afterLockup);
                    let amount = 50006;


                    await token.mint(a1, amount, {from: admin});
                    const beforeBalance = await token.balanceOf(a1);
                    await token.transfer(k1, amount, {from: a1});
                    const receiverBalance = await token.balanceOf(a1);
                    assert(receiverBalance.eq(beforeBalance))
                });

            });

            describe('1 year after the sale ends', function () {

                it('transfer accredited <-> accredited', async function () {

                    await token.finishTokenSale({from: admin});
                    const tokenSaleFinished = await token.tokenSaleFinished();
                    const tokenSaleEndTime = await token.tokenSaleEndTime();
                    let afterLockup = parseInt(tokenSaleEndTime) + duration.years(1) + 10;
                    await increaseTimeTo(afterLockup);

                    assert(tokenSaleFinished);
                    assert(!tokenSaleEndTime.eq(0))
                    let amount = 40000000000000;


                    await token.mint(a1, amount, {from: admin});
                    await token.transfer(a2, amount, {from: a1});
                    const receiverBalance = await token.balanceOf(a2);
                    assert(receiverBalance.eq(amount))
                });

                it('transfer kyc <-> kyc', async function () {

                    await token.finishTokenSale({from: admin});
                    const tokenSaleEndTime = await token.tokenSaleEndTime();
                    assert(!tokenSaleEndTime.eq(0))
                    let afterLockup = parseInt(tokenSaleEndTime) + duration.years(1) + 10;
                    await increaseTimeTo(afterLockup);
                    let amount = 20001;


                    await token.mint(k1, amount, {from: admin});
                    await token.transfer(k2, amount, {from: k1});


                    const receiverBalance = await token.balanceOf(k2);
                    assert(receiverBalance.eq(amount))
                });

                it('transfer admin <-> kyc', async function () {

                    await token.finishTokenSale({from: admin});
                    const tokenSaleEndTime = await token.tokenSaleEndTime();
                    assert(!tokenSaleEndTime.eq(0))
                    let afterLockup = parseInt(tokenSaleEndTime) + duration.years(1) + 10;
                    await increaseTimeTo(afterLockup);
                    let amount = 30001;


                    await token.mint(admin, amount, {from: admin});
                    await token.transfer(k1, amount, {from: admin});


                    const receiverBalance = await token.balanceOf(k1);
                    assert(receiverBalance.eq(amount))
                });

                it('transfer kyc <-> accredited', async function () {

                    await token.finishTokenSale({from: admin});
                    const tokenSaleEndTime = await token.tokenSaleEndTime();
                    assert(!tokenSaleEndTime.eq(0))
                    let afterLockup = parseInt(tokenSaleEndTime) + duration.years(1) + 10;
                    await increaseTimeTo(afterLockup);
                    let amount = 50002;


                    await token.mint(k1, amount, {from: admin});

                    let {logs} = await token.transfer(a2, amount, {from: k1});
                    const receiverBalance = await token.balanceOf(a2);
                    assert(receiverBalance.eq(amount))
                });

                it('transfer accredited <-> kyc', async function () {

                    await token.finishTokenSale({from: admin});
                    const tokenSaleEndTime = await token.tokenSaleEndTime();
                    assert(!tokenSaleEndTime.eq(0))
                    let afterLockup = parseInt(tokenSaleEndTime) + duration.years(1) + 10;
                    await increaseTimeTo(afterLockup);
                    let amount = 50003;


                    await token.mint(a1, amount, {from: admin});

                    let {logs} = await token.transfer(k2, amount, {from: a1});
                    const receiverBalance = await token.balanceOf(k2);
                    assert(receiverBalance.eq(amount))
                });

                it('fail transfer accredited <-> unauthorized', async function () {

                    await token.finishTokenSale({from: admin});
                    const tokenSaleEndTime = await token.tokenSaleEndTime();
                    assert(!tokenSaleEndTime.eq(0))
                    let afterLockup = parseInt(tokenSaleEndTime) + duration.years(1) + 10;
                    await increaseTimeTo(afterLockup);
                    let amount = 50003;


                    await token.mint(a1, amount, {from: admin});
                    assertRevert(token.transfer(other2, amount, {from: a1}));
                });

                it('fail transfer kyc <-> unauthorized', async function () {

                    await token.finishTokenSale({from: admin});
                    const tokenSaleEndTime = await token.tokenSaleEndTime();
                    assert(!tokenSaleEndTime.eq(0))
                    let afterLockup = parseInt(tokenSaleEndTime) + duration.years(1) + 10;
                    await increaseTimeTo(afterLockup);
                    let amount = 50003;


                    await token.mint(k1, amount, {from: admin});
                    assertRevert(token.transfer(other2, amount, {from: k1}));
                });

                it('fail transfer admin <-> unauthorized', async function () {

                    await token.finishTokenSale({from: admin});
                    const tokenSaleEndTime = await token.tokenSaleEndTime();
                    assert(!tokenSaleEndTime.eq(0))
                    let afterLockup = parseInt(tokenSaleEndTime) + duration.years(1) + 10;
                    await increaseTimeTo(afterLockup);
                    let amount = 50003;


                    await token.mint(admin, amount, {from: admin});
                    assertRevert(token.transfer(other2, amount, {from: admin}));
                });

                it('fail transfer unauthorized <-> admin', async function () {

                    await token.finishTokenSale({from: admin});
                    const tokenSaleEndTime = await token.tokenSaleEndTime();
                    assert(!tokenSaleEndTime.eq(0))
                    let afterLockup = parseInt(tokenSaleEndTime) + duration.years(1) + 10;
                    await increaseTimeTo(afterLockup);
                    let amount = 50003;

                    assertRevert(token.transfer(admin, amount, {from: other2}));
                });

                it('fail transfer unauthorized <-> kyc', async function () {

                    await token.finishTokenSale({from: admin});
                    const tokenSaleEndTime = await token.tokenSaleEndTime();
                    assert(!tokenSaleEndTime.eq(0))
                    let afterLockup = parseInt(tokenSaleEndTime) + duration.years(1) + 10;
                    await increaseTimeTo(afterLockup);
                    let amount = 50003;

                    assertRevert(token.transfer(k1, amount, {from: other2}));
                });

                it('fail transfer unauthorized <-> accredited', async function () {

                    await token.finishTokenSale({from: admin});
                    const tokenSaleEndTime = await token.tokenSaleEndTime();
                    assert(!tokenSaleEndTime.eq(0))
                    let afterLockup = parseInt(tokenSaleEndTime) + duration.years(1) + 10;
                    await increaseTimeTo(afterLockup);
                    let amount = 50003;

                    assertRevert(token.transfer(a1, amount, {from: other2}));
                });

                it('fail transfer unauthorized <-> unauthorized', async function () {

                    await token.finishTokenSale({from: admin});
                    const tokenSaleEndTime = await token.tokenSaleEndTime();
                    assert(!tokenSaleEndTime.eq(0))
                    let afterLockup = parseInt(tokenSaleEndTime) + duration.years(1) + 10;
                    await increaseTimeTo(afterLockup);
                    let amount = 50003;

                    assertRevert(token.transfer(admin, amount, {from: other2}));
                });


            });

        });


    })

});