pragma solidity ^0.4.18;


import "zeppelin-solidity/contracts/ownership/Ownable.sol";


/**
 * @title Access Control contract
 * @dev Manages contracts roles
 */
contract AccessControl is Ownable {

    mapping (address => uint8) public accounts;

    event AccessControlAccountAdded(address addr,uint8 role);
    event AccessControlAccountRemoved(address addr);

    /**
     * @dev add an address to the access control
     * @param addr address
     * @param role the role
     * @return true if the address was added to the access control, false if the address was already in the access control
     */
    function addAddressToAccessControl(address addr, uint8 role) onlyOwner public returns(bool success) {
        if (accounts[addr] == 0x0) {
            accounts[addr] = role;
            AccessControlAccountAdded(addr, role);
            success = true;
        }
    }

    /**
     * @dev add addresses to the access control
     * @param addrs addresses
     * @return true if at least one address was added to the access control,
     * false if all addresses were already in the access control
     */
    function addAddressesToAccessControl(address[] addrs, uint8[] roles) onlyOwner public returns(bool success) {
        for (uint256 i = 0; i < addrs.length; i++) {
            if (addAddressToAccessControl(addrs[i],roles[i])) {
                success = true;
            }
        }
    }

    /**
     * @dev remove an address from the access control
     * @param addr address
     * @return true if the address was removed from the access control,
     * false if the address wasn't in the access control in the first place
     */
    function removeAddressFromAccessControl(address addr) onlyOwner public returns(bool success) {
        if (accounts[addr] != 0) {
            accounts[addr] = 0;
            AccessControlAccountRemoved(addr);
            success = true;
        }
    }

    /**
     * @dev remove addresses from the access control
     * @param addrs addresses
     * @return true if at least one address was removed from the access control,
     * false if all addresses weren't in the access control in the first place
     */
    function removeAddressesFromAccessControl(address[] addrs) onlyOwner public returns(bool success) {
        for (uint256 i = 0; i < addrs.length; i++) {
            if (removeAddressFromAccessControl(addrs[i])) {
                success = true;
            }
        }
    }


    /**
     * @dev get the role of address
     * @param addr addresse
     * @return the role of this address
     * 0 if the role not exists
     */
    function getRole(address addr) public view returns(uint8 role){
        return accounts[addr];
    }

}