pragma solidity ^0.4.0;

import "./AccessControl.sol";
import "./VanywhereToken.sol";
import "zeppelin-solidity/contracts/crowdsale/Crowdsale.sol";


contract VanywhereCrowdsale is Crowdsale{

    AccessControl public acm;

    uint8 public KYC_USER = 1;

    uint8 public ACCREDITED = 2;

    //Time limits
    uint public constant STAGE_ONE_TIME_END = 5 hours;
    uint public constant STAGE_TWO_TIME_END = 72 hours;
    uint public constant STAGE_THREE_TIME_END = 2 weeks;
    uint public constant STAGE_FOUR_TIME_END = 4 weeks;
    //Prices of VANY
    uint public constant PRICE_STAGE_ONE   = 210;
    uint public constant PRICE_STAGE_TWO   = 200;
    uint public constant PRICE_STAGE_THREE = 190;
    uint public constant PRICE_STAGE_FOUR  = 185;


    uint256 public openingTime;
    uint256 public closingTime;







    function VanywhereCrowdsale(uint256 _startRate, address _wallet, ERC20 _token, uint256 _openingTime, uint256 _closingTime, AccessControl addr)
    Crowdsale(PRICE_STAGE_ONE, _wallet, _token)
    public {
        acm = AccessControl(addr);
        openingTime = _openingTime;
        closingTime = _closingTime;
    }


    /**
     * @dev Reverts if beneficiary is not whitelisted. Can be used when extending this contract.
     */
    modifier isWhitelisted(address _beneficiary) {
        require(acm.getRole(msg.sender) == ACCREDITED || acm.getRole(msg.sender) == KYC_USER);
        _;
    }


    /**
     * @dev Reverts if not in crowdsale time range.
     */
    modifier onlyWhileOpen {
        require(now >= openingTime && now <= closingTime);
        _;
    }


    /**
     * @dev Overrides parent method taking into account variable rate.
     * @param _weiAmount The value in wei to be converted into tokens
     *    @return The number of tokens _weiAmount wei will buy at present time
     */
    function _getTokenAmount(uint256 _weiAmount) internal view returns (uint256) {
        if (now <= openingTime + STAGE_ONE_TIME_END) return _weiAmount.mul(PRICE_STAGE_ONE);
        if (now <= openingTime + STAGE_TWO_TIME_END) return _weiAmount.mul(PRICE_STAGE_TWO);
        if (now <= openingTime + STAGE_THREE_TIME_END) return _weiAmount.mul(PRICE_STAGE_THREE);
        if (now <= openingTime + STAGE_FOUR_TIME_END) return _weiAmount.mul(PRICE_STAGE_FOUR);
        else return 0;
    }

    /**
     * @dev Overrides delivery by minting tokens upon purchase.
     * @param _beneficiary Token purchaser
     * @param _tokenAmount Number of tokens to be minted
     */
    function _deliverTokens(address _beneficiary, uint256 _tokenAmount) internal {
        require(VanywhereToken(token).mint(_beneficiary, _tokenAmount));
    }

    /**
     * @dev Checks whether the period in which the crowdsale is open has already elapsed.
     * @return Whether crowdsale period has elapsed
     */
    function hasClosed() public view returns (bool) {
        return now > closingTime;
    }


    /**
     * @dev Extend parent behavior requiring beneficiary to be in whitelist.
     * @param _beneficiary Token beneficiary
     * @param _weiAmount Amount of wei contributed
     */
    function _preValidatePurchase(address _beneficiary, uint256 _weiAmount) internal onlyWhileOpen isWhitelisted(_beneficiary) {
        super._preValidatePurchase(_beneficiary, _weiAmount);
    }


}
