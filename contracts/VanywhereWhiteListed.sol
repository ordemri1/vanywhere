pragma solidity ^0.4.0;


import "zeppelin-solidity/contracts/ownership/Ownable.sol";
import "./AccessControl.sol";

// VANYWHERE ROLES
// 0 - not exist
// 1 - kyc only
// 2 - accredited investor
// 8 - crowdsale
// 9 - admin
contract VanywhereWhiteListed is Ownable {

    uint8 public KYC_USER = 1;

    uint8 public ACCREDITED = 2;

    uint8 public CROWDSALE = 8;

    uint8 public ADMIN = 9;

    uint public ACCREDITED_LOCKUP = 365 days;

    uint public LOCKUP = 90 days;


    AccessControl public acm;

    uint public tokenSaleEndTime;

    bool public tokenSaleFinished = false;

    function VanywhereWhiteListed(AccessControl addr) public {
        acm = AccessControl(addr);
    }


    modifier canTransfer(address from, address to){
        if (tokenSaleFinished && validateTransfer(from, to))
        _;

    }

    function isAccredited(address addr) view internal returns (bool){
        return acm.getRole(addr) == ACCREDITED;
    }

    function validateTransfer(address from, address to) view public returns (bool){
        if (now >= tokenSaleEndTime + ACCREDITED_LOCKUP) {
            return true;
        }
        else if (now >= tokenSaleEndTime + LOCKUP) {
            return isAccredited(from) == isAccredited(to);
        }
        return false;
    }


    function finishTokenSale() public onlyOwner returns (bool success){
        if (!tokenSaleFinished) {
            tokenSaleFinished = true;
            tokenSaleEndTime = now;
            return true;
        }
    }


}
