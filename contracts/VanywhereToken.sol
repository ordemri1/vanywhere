pragma solidity ^0.4.4;


import "zeppelin-solidity/contracts/token/ERC20/DetailedERC20.sol";
import "zeppelin-solidity/contracts/token/ERC20/CappedToken.sol";
import "./AccessControl.sol";
import "./VanywhereWhiteListed.sol";


/**
 * @title Vanywhere QRC20 contract
 * @dev vanywhere token
 */
contract VanywhereToken is StandardToken, Ownable, VanywhereWhiteListed {
    function VanywhereToken(AccessControl addr, uint _cap, string _name, string _symbol, uint8 _decimals)
    VanywhereWhiteListed(addr)
    public {
        name = _name;
        symbol = _symbol;
        decimals = _decimals;
        cap = _cap;
    }


    event Mint(address indexed to, uint256 amount);
    event MintFinished();

    uint256 public cap;
    string public name;
    string public symbol;
    uint8 public decimals;

    bool public mintingFinished = false;


    modifier canMint() {
        require(!mintingFinished && (acm.getRole(msg.sender) == CROWDSALE || acm.getRole(msg.sender) == ADMIN));
        _;
    }

    modifier capNotReached(uint256 _value){
        require(totalSupply_.add(_value) <= cap);
        _;
    }


    function transfer(address _to, uint256 _value) public canTransfer(msg.sender, _to) returns (bool) {
        return super.transfer(_to, _value);
    }

    function transferFrom(address _from, address _to, uint256 _value) public canTransfer(_from, _to) returns (bool) {
        return super.transferFrom(_from, _to, _value);
    }

    /**
  * @dev Function to mint tokens
  * @param _to The address that will receive the minted tokens.
  * @param _amount The amount of tokens to mint.
  * @return A boolean that indicates if the operation was successful.
  */
    function mint(address _to, uint256 _amount) canMint capNotReached(_amount) public returns (bool) {
        totalSupply_ = totalSupply_.add(_amount);
        balances[_to] = balances[_to].add(_amount);
        Mint(_to, _amount);
        Transfer(address(0), _to, _amount);
        return true;
    }

    /**
   * @dev Function to stop minting new tokens.
   * @return True if the operation was successful.
   */
    function finishMinting() onlyOwner canMint public returns (bool) {
        mintingFinished = true;
        MintFinished();
        return true;
    }

}
